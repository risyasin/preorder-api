<?php
declare(strict_types = 1);
/**
 * /tests/Functional/Controller/OrderControllerTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Functional\Controller;

use App\Utils\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PreorderControllerTest
 *
 * @package App\Tests\Functional\Controller
 * @author  yasin inat
 */
class PreorderControllerTest extends WebTestCase
{
    private $baseUrl = '/preorder';

    /**
     * @throws \Exception
     */
    public function testThatGetBaseRouteReturn401(): void
    {
        $client = $this->getClient();
        $client->request('GET', $this->baseUrl);

        $response = $client->getResponse();

        static::assertInstanceOf(Response::class, $response);

        /** @noinspection NullPointerExceptionInspection */
        static::assertSame(401, $response->getStatusCode());
    }
}
