<?php
declare(strict_types = 1);
/**
 * /tests/Functional/Controller/CustomerControllerTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Functional\Controller;

use App\Utils\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CustomerControllerTest
 *
 * @package App\Tests\Functional\Controller
 * @author  yasin inat
 */
class CustomerControllerTest extends WebTestCase
{
    private $baseUrl = '/customer';

    /**
     * @throws \Exception
     */
    public function testThatGetBaseRouteReturn401(): void
    {
        $client = $this->getClient();
        $client->request('GET', $this->baseUrl);

        $response = $client->getResponse();

        static::assertInstanceOf(Response::class, $response);

        /** @noinspection NullPointerExceptionInspection */
        static::assertSame(401, $response->getStatusCode());
    }
}
