<?php
declare(strict_types = 1);
/**
 * /tests/Integration/DTO/CustomerTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\DTO;

use App\DTO\Customer as CustomerDto;
use App\Entity\Customer as CustomerEntity;

/**
 * Class CustomerTest
 *
 * @package App\Tests\Integration\DTO
 * @author  yasin inat
 */
class CustomerTest extends DtoTestCase
{
    protected $dtoClass = CustomerDto::class;

    public function testThatLoadMethodWorks(): void
    {
        // Create entity
        $entity = new CustomerEntity();

        // Create DTO and load entity
        $dto = new CustomerDto();
        $dto->load($entity);

        static::assertSame($entity->getId(), $dto->getId());
    }
}
