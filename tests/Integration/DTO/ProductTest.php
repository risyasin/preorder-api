<?php
declare(strict_types = 1);
/**
 * /tests/Integration/DTO/ProductTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\DTO;

use App\DTO\Product as ProductDto;
use App\Entity\Product as ProductEntity;

/**
 * Class ProductTest
 *
 * @package App\Tests\Integration\DTO
 * @author  yasin inat
 */
class ProductTest extends DtoTestCase
{
    protected $dtoClass = ProductDto::class;

    public function testThatLoadMethodWorks(): void
    {
        // Create entity
        $entity = new ProductEntity();

        // Create DTO and load entity
        $dto = new ProductDto();
        $dto->load($entity);

        static::assertSame($entity->getId(), $dto->getId());
    }
}
