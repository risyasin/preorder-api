<?php
declare(strict_types = 1);
/**
 * /tests/Integration/DTO/OrderTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\DTO;

use App\DTO\Preorder as OrderDto;
use App\Entity\Preorder as OrderEntity;

/**
 * Class OrderTest
 *
 * @package App\Tests\Integration\DTO
 * @author  yasin inat
 */
class OrderTest extends DtoTestCase
{
    protected $dtoClass = OrderDto::class;

    public function testThatLoadMethodWorks(): void
    {
        // Create entity
        $entity = new OrderEntity();

        // Create DTO and load entity
        $dto = new OrderDto();
        $dto->load($entity);

        static::assertSame($entity->getId(), $dto->getId());
    }
}
