<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Entity//ProductTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\Entity;

use App\Entity\Product;

/**
 * Class ProductTest
 *
 * @package App\Tests\Integration\Entity
 * @author  yasin inat
 */
class ProductTest extends EntityTestCase
{
    /**
     * @var string
     */
    protected $entityName = Product::class;
}
