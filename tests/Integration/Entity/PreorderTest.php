<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Entity//OrderTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\Entity;

use App\Entity\Preorder;

/**
 * Class OrderTest
 *
 * @package App\Tests\Integration\Entity
 * @author  yasin inat
 */
class OrderTest extends EntityTestCase
{
    /**
     * @var string
     */
    protected $entityName = Preorder::class;
}
