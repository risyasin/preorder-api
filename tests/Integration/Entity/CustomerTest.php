<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Entity//CustomerTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\Entity;

use App\Entity\Customer;

/**
 * Class CustomerTest
 *
 * @package App\Tests\Integration\Entity
 * @author  yasin inat
 */
class CustomerTest extends EntityTestCase
{
    /**
     * @var string
     */
    protected $entityName = Customer::class;
}
