<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Controller/ProductControllerTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\Controller;

use App\Controller\ProductController;
use App\Resource\ProductResource;
use App\Utils\Tests\RestIntegrationControllerTestCase;

/**
 * Class ProductControllerTest
 *
 * @package Integration\Controller
 * @author  yasin inat
 */
class ProductControllerTest extends RestIntegrationControllerTestCase
{
    /**
     * @var string
     */
    protected $controllerClass = ProductController::class;

    /**
     * @var string
     */
    protected $resourceClass = ProductResource::class;
}
