<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Controller/CustomerControllerTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\Controller;

use App\Controller\CustomerController;
use App\Resource\CustomerResource;
use App\Utils\Tests\RestIntegrationControllerTestCase;

/**
 * Class CustomerControllerTest
 *
 * @package Integration\Controller
 * @author  yasin inat
 */
class CustomerControllerTest extends RestIntegrationControllerTestCase
{
    /**
     * @var string
     */
    protected $controllerClass = CustomerController::class;

    /**
     * @var string
     */
    protected $resourceClass = CustomerResource::class;
}
