<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Controller/PreorderControllerTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\Controller;

use App\Controller\OrderController;
use App\Resource\PreorderResource;
use App\Utils\Tests\RestIntegrationControllerTestCase;

/**
 * Class OrderControllerTest
 *
 * @package Integration\Controller
 * @author  yasin inat
 */
class PreorderControllerTest extends RestIntegrationControllerTestCase
{
    /**
     * @var string
     */
    protected $controllerClass = PreorderController::class;

    /**
     * @var string
     */
    protected $resourceClass = PreorderResource::class;
}
