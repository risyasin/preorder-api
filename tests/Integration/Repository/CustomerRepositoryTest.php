<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Integration/CustomerRepositoryTest.php
 *
* @author  yasin inat
 */
namespace App\Tests\Integration\Repository;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use App\Resource\CustomerResource;

/**
 * Class CustomerRepositoryTest
 *
 * @package App\Tests\Integration\Repository
 * @author  yasin inat
 */
class CustomerRepositoryTest extends RepositoryTestCase
{
    /**
     * @var string
     */
    protected $entityName = Customer::class;

    /**
     * @var string
     */
    protected $repositoryName = CustomerRepository::class;

    /**
     * @var string
     */
    protected $resourceName = CustomerResource::class;

    /**
     * @var array
     */
    protected $associations = [
        'createdBy',
        'updatedBy',
    ];
}
