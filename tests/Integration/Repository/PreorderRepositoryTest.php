<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Integration/OrderRepositoryTest.php
 *
* @author  yasin inat
 */
namespace App\Tests\Integration\Repository;

use App\Entity\Preorder;
use App\Repository\OrderRepository;
use App\Resource\PreorderResource;

/**
 * Class OrderRepositoryTest
 *
 * @package App\Tests\Integration\Repository
 * @author  yasin inat
 */
class OrderRepositoryTest extends RepositoryTestCase
{
    /**
     * @var string
     */
    protected $entityName = Preorder::class;

    /**
     * @var string
     */
    protected $repositoryName = OrderRepository::class;

    /**
     * @var string
     */
    protected $resourceName = PreorderResource::class;

    /**
     * @var array
     */
    protected $associations = [
        'createdBy',
        'updatedBy',
    ];
}
