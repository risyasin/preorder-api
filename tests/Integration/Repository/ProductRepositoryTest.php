<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Integration/ProductRepositoryTest.php
 *
* @author  yasin inat
 */
namespace App\Tests\Integration\Repository;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Resource\ProductResource;

/**
 * Class ProductRepositoryTest
 *
 * @package App\Tests\Integration\Repository
 * @author  yasin inat
 */
class ProductRepositoryTest extends RepositoryTestCase
{
    /**
     * @var string
     */
    protected $entityName = Product::class;

    /**
     * @var string
     */
    protected $repositoryName = ProductRepository::class;

    /**
     * @var string
     */
    protected $resourceName = ProductResource::class;

    /**
     * @var array
     */
    protected $associations = [
        'createdBy',
        'updatedBy',
    ];
}
