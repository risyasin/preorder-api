<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Resource/ProductResourceTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\Resource;

use App\Entity\Product;
use App\Repository\ProductRepository;
use App\Resource\ProductResource;

/**
 * Class ProductResourceTest
 *
 * @package App\Tests\Integration\Resource
 * @author  yasin inat
 */
class ProductResourceTest extends ResourceTestCase
{
    protected $entityClass = Product::class;
    protected $resourceClass = ProductResource::class;
    protected $repositoryClass = ProductRepository::class;
}
