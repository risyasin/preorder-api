<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Resource/OrderResourceTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\Resource;

use App\Entity\Preorder;
use App\Repository\OrderRepository;
use App\Resource\PreorderResource;

/**
 * Class OrderResourceTest
 *
 * @package App\Tests\Integration\Resource
 * @author  yasin inat
 */
class OrderResourceTest extends ResourceTestCase
{
    protected $entityClass = Preorder::class;
    protected $resourceClass = PreorderResource::class;
    protected $repositoryClass = OrderRepository::class;
}
