<?php
declare(strict_types = 1);
/**
 * /tests/Integration/Resource/CustomerResourceTest.php
 *
 * @author  yasin inat
 */
namespace App\Tests\Integration\Resource;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use App\Resource\CustomerResource;

/**
 * Class CustomerResourceTest
 *
 * @package App\Tests\Integration\Resource
 * @author  yasin inat
 */
class CustomerResourceTest extends ResourceTestCase
{
    protected $entityClass = Customer::class;
    protected $resourceClass = CustomerResource::class;
    protected $repositoryClass = CustomerRepository::class;
}
