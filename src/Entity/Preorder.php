<?php
declare(strict_types = 1);
/**
 * /src/Entity/Order.php
 *
 * @author  yasin inat
 */
namespace App\Entity;

use App\Entity\Traits\Blameable;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Preorder
 *
 * @ORM\Table(
 *      name="preorder",
 *  )
 * @ORM\Entity()
 *
 * @package App\Entity
 * @author  yasin inat
 */
class Preorder implements EntityInterface
{
    // Traits
    use Blameable;
    use Timestampable;

    /**
     * @var string
     *
     * @Groups({
     *      "Preorder",
     *      "Preorder.id",
     *  })
     *
     * @ORM\Column(
     *      name="id",
     *      type="guid",
     *      nullable=false,
     *  )
     * @ORM\Id()
     */
    private $id;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('approved', 'rejected', 'autoRejected')")
     * @Assert\NotBlank
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity="Product")
     * @ORM\JoinTable(name="preorders_products",
     *      joinColumns={@ORM\JoinColumn(name="preorder_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", unique=true)}
     *      )
     * @Assert\NotBlank
     */
    private $products;


    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
        $this->products = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
