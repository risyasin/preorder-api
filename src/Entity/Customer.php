<?php
declare(strict_types = 1);
/**
 * /src/Entity/Customer.php
 *
 * @author  yasin inat
 */
namespace App\Entity;

use App\Entity\Traits\Blameable;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Customer
 *
 * @ORM\Table(
 *      name="customer",
 *  )
 * @ORM\Entity()
 *
 * @package App\Entity
 * @author  yasin inat
 */
class Customer implements EntityInterface
{
    // Traits
    use Blameable;
    use Timestampable;

    /**
     * @var string
     *
     * @Groups({
     *      "Customer",
     *      "Customer.id",
     *  })
     *
     * @ORM\Column(
     *      name="id",
     *      type="guid",
     *      nullable=false,
     *  )
     * @ORM\Id()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank
     * @Assert\Email
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=15, unique=true)
     * @Assert\NotBlank
     */
    private $phone;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4()->toString();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
