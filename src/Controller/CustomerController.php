<?php
declare(strict_types = 1);
/**
 * /src/Controller/CustomerController.php
 *
 * @author  yasin inat
 */
namespace App\Controller;

use App\Resource\CustomerResource;
use App\Rest\Controller;
use App\Rest\ResponseHandler;
use App\Rest\Traits\Actions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;

/**
 * @Route(path="/customer")
 *
 * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
 *
 * @SWG\Tag(name="Customer")
 *
 * @package App\Controller
 * @author  yasin inat
 *
 * @method CustomerResource getResource()
 */
class CustomerController extends Controller
{
    // Traits for REST actions
    use Actions\User\CountAction;
    use Actions\User\FindAction;
    use Actions\User\FindOneAction;
    use Actions\User\IdsAction;
    use Actions\User\CreateAction;
    use Actions\User\DeleteAction;
    use Actions\User\PatchAction;
    use Actions\User\UpdateAction;

    /**
     * CustomerController constructor.
     *
     * @param CustomerResource $resource
     * @param ResponseHandler $responseHandler
     */
    public function __construct(CustomerResource $resource, ResponseHandler $responseHandler)
    {
        $this->init($resource, $responseHandler);
    }
}
