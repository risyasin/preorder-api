<?php
declare(strict_types = 1);
/**
 * /src/Repository/ProductRepository.php
 *
 * @author  yasin inat
 */
namespace App\Repository;

use App\Entity\Product as Entity;

/** @noinspection PhpHierarchyChecksInspection */
/**
 * Class ProductRepository
 *
 * @package App\Repository
 * @author  yasin inat *
 * @codingStandardsIgnoreStart
 *
 * @method Entity|null find(string $id, string $lockMode = null, string $lockVersion = null)
 * @method Entity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entity[]    findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null)
 * @method Entity[]    findByAdvanced(array $criteria, array $orderBy = null, int $limit = null, int $offset = null, array $search = null): array
 * @method Entity[]    findAll()
 *
 * @codingStandardsIgnoreEnd
 */
class ProductRepository extends BaseRepository
{
    /**
     * @var string
     */
    protected static $entityName = Entity::class;
}
