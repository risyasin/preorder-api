<?php
declare(strict_types = 1);
/**
* /src/DTO/Preorder.php
*
* @author  yasin inat
*/
namespace App\DTO;

use App\Entity\EntityInterface;
use App\Entity\Preorder as OrderEntity;

/**
 * Class Order
 *
 * @package App\DTO
 * @author  yasin inat
 */
class Preorder extends RestDto
{
    /**
     * @var string|null
     */
    protected $id;

    /**
     * Method to load DTO data from specified entity.
     *
     * @param EntityInterface||OrderEntity $entity
     *
     * @return RestDtoInterface|Preorder
     */
    public function load(EntityInterface $entity): RestDtoInterface
    {
        $this->id = $entity->getId();

        return $this;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param null|string $id
     *
     * @return Preorder
     */
    public function setId(string $id = null): Preorder
    {
        $this->setVisited('id');

        $this->id = $id;

        return $this;
    }
}
