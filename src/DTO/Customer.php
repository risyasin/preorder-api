<?php
declare(strict_types = 1);
/**
* /src/DTO/Customer.php
*
* @author  yasin inat
*/
namespace App\DTO;

use App\Entity\EntityInterface;
use App\Entity\Customer as CustomerEntity;

/**
 * Class Customer
 *
 * @package App\DTO
 * @author  yasin inat
 */
class Customer extends RestDto
{
    /**
     * @var string|null
     */
    protected $id;

    /**
     * Method to load DTO data from specified entity.
     *
     * @param EntityInterface||CustomerEntity $entity
     *
     * @return RestDtoInterface|Customer
     */
    public function load(EntityInterface $entity): RestDtoInterface
    {
        $this->id = $entity->getId();

        return $this;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param null|string $id
     *
     * @return Customer
     */
    public function setId(string $id = null): Customer
    {
        $this->setVisited('id');

        $this->id = $id;

        return $this;
    }
}
