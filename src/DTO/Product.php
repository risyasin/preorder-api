<?php
declare(strict_types = 1);
/**
* /src/DTO/Product.php
*
* @author  yasin inat
*/
namespace App\DTO;

use App\Entity\EntityInterface;
use App\Entity\Product as ProductEntity;

/**
 * Class Product
 *
 * @package App\DTO
 * @author  yasin inat
 */
class Product extends RestDto
{
    /**
     * @var string|null
     */
    protected $id;

    /**
     * Method to load DTO data from specified entity.
     *
     * @param EntityInterface||ProductEntity $entity
     *
     * @return RestDtoInterface|Product
     */
    public function load(EntityInterface $entity): RestDtoInterface
    {
        $this->id = $entity->getId();

        return $this;
    }

    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param null|string $id
     *
     * @return Product
     */
    public function setId(string $id = null): Product
    {
        $this->setVisited('id');

        $this->id = $id;

        return $this;
    }
}
